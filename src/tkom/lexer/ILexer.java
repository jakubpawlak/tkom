package tkom.lexer;

public interface ILexer {
    Symbol nextSymbol();
}
